
public class invoker {
	
private dominoCommand place;
	
	public invoker(dominoCommand dCommand) {
		this.place = dCommand;
	}
	
	public void doPlaceDomino() {
		place.execute();
	}
	
	public void undoPlaceDomino() {
		place.unexecute();
	}
}
