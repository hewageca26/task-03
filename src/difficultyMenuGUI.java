import java.awt.GridLayout;

import javax.swing.JButton;

public class difficultyMenuGUI extends Observer{
	public difficultyMenuGUI(Subject subject){
	      this.subject = subject;
	      this.subject.attach(this);
	   }
			
	   @Override
	   public void display() {
		   difficultyMenu.setLayout(new GridLayout(0, 1));
		   button = new JButton("Simples");
		   difficultyMenu.add(button);
		   button.addActionListener(this); 
		   button = new JButton("Not-so-simples");
		   difficultyMenu.add(button);
		   button.addActionListener(this); 
		   button = new JButton("Super-duper-shuffled");
		   difficultyMenu.add(button);
		   button.addActionListener(this); 
		   difficultyMenu.pack();
		   difficultyMenu.setLocationRelativeTo(null);
		   aardvark2 = new Aardvark(level);
	   }
}
