import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class welcomeMenu extends Observer{
	
	public welcomeMenu(Subject subject){
	      this.subject = subject;
	      this.subject.attach(this);
	   }
	
	@Override
	   public void display() {
			welcome.setLayout(new GridLayout(0, 1));
			welcome.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			welcome.setResizable(false);
			welcome.setLocationRelativeTo(null); 
			welcome.setVisible(true);
			label = new JLabel(" Welcome To Abominodo - The Best Dominoes Puzzle Game in the Universe ");
			welcome.add(label);
			label = new JLabel(" Version 1.0 (c), Kevan Buckley, 2010");
			welcome.add(label);	
			label = new JLabel(MultiLinugualStringTable.getMessage(0));
			welcome.add(label);
			text = new JTextField(); 
			welcome.add(text);
		    button = new JButton("Start");
		    welcome.add(button);
			button.addActionListener(this); 
			welcome.pack();
	   }

}
