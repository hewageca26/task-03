import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;

public class mainMenuGUI extends Observer{
	public mainMenuGUI(Subject subject){
	      this.subject = subject;
	      this.subject.attach(this);
	   }
			
	   @Override
	   public void display() {
		   mainMenu.setLayout(new GridLayout(0, 1));
		   label = new JLabel(MultiLinugualStringTable.getMessage(1) + ". " + MultiLinugualStringTable.getMessage(2));
		   mainMenu.add(label);
		   button = new JButton("Play");
		   mainMenu.add(button);
		   button.addActionListener(this); 
		   button = new JButton("High Scores");
		   mainMenu.add(button);
		   button.addActionListener(this); 
		   button = new JButton("Rules");
		   mainMenu.add(button);
		   button.addActionListener(this); 
		   button = new JButton("Quit");
		   mainMenu.add(button);
		   button.addActionListener(this); 
		   mainMenu.setLocationRelativeTo(null); 
		   mainMenu.pack();
	   }
}
