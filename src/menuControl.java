
public class menuControl {
	private username model;
	private menuViewer view;
	
	public menuControl(username model, menuViewer view) {
		this.model = model;
		this.view = view;
	}
	
	public void setPlayerName(String name) {
		model.setName(name);
	}
	
	public String getPlayerName() {
		return model.getName();
	}
	
	public void updateView() {
		view.displayGUI();
	}
}
