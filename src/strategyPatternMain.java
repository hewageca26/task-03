
public class strategyPatternMain {
	public static void main(String [] args) {
		context cont = new context(new autoplay1());		
		cont.executeStr();

		cont = new context(new autoplay2());		
	    cont.executeStr();

	    cont = new context(new autoplay3());		
	    cont.executeStr();
	}
}
