import javax.swing.JOptionPane;

public class commandPatternMain {
	public static void main(String [] args) {
		Aardvark ardvark = new Aardvark(1);
		
		dominoCommand place = new dominoCommand(ardvark);
		
		invoker commandInvoker = new invoker(place);
		commandInvoker.doPlaceDomino();
		
		if (JOptionPane.showConfirmDialog(null, "Do you want to undo command?", "UNDO",
		        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			commandInvoker.undoPlaceDomino();
		} 

	}
}
